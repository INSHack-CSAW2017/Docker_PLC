FROM arm32v7/debian:stretch-slim

# basic flask environment
RUN apt-get update && \
	apt-get install -y git nginx uwsgi python-pip python-dev uwsgi-plugin-python
RUN python -m pip install --upgrade pip
RUN pip install flask  sysv_ipc jsonrpcserver

COPY PLCServer/ /app

# app dir
RUN chown -R www-data:www-data /app
RUN mkdir /run/uwsgi
RUN chown -R www-data:www-data /run/uwsgi


COPY plc/ /plc

# expose web server port
# only http, for ssl use reverse proxy
EXPOSE 80

# copy config files into filesystem
COPY nginx.conf /etc/nginx/nginx.conf
COPY app.ini /app.ini
COPY entrypoint.sh /entrypoint.sh

# exectute start up script
ENTRYPOINT ["/entrypoint.sh"]
