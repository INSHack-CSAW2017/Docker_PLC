from sharememctl import PlcMemory
from flask import Flask, request, Response
from jsonrpcserver import methods
from jsonrpcserver.exceptions import InvalidParams, ServerError

app = Flask(__name__)

plc = PlcMemory(2516)

"""
This method refresh all the values set in plc
"""
@methods.add
def refresh():
    plc.Refresh()
    return 'ok'

"""
This method set a specific value in the input
"""
@methods.add
def set_bool_input(**kwargs):
    if 'index' not in kwargs:
        raise InvalidParams('Register index from 1 to 14 is required')
    if 'value' not in kwargs:
        raise InvalidParams('value is required')
    index = kwargs['index']
    value = kwargs['value']
    if (type(index)) != int:
        raise InvalidParams('Index must be an int')
    if (type(value)) != bool:
        raise InvalidParams('Value must be a boolean')
    plc.SetBoolInput(index-1,value)
    return 'ok'


"""
This method recover all the inputs of the plc
It also refresh the plc values
"""
@methods.add
def get_outputs():
    plc.Refresh()
    return (plc.ReadBoolOutputs(),plc.ReadIntOutput())

"""
This method reset all the bool inputs in plc
"""
@methods.add
def reset_inputs():
    return plc.Reset()

# Set JsonRPC Server
@app.route('/', methods=['POST'])
def index():
    req = request.get_data().decode()
    response = methods.dispatch(req)
    return Response(str(response), response.http_status,mimetype='application/json')

if __name__ == '__main__':
    app.run()
