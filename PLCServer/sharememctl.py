import sys
import binascii
import sysv_ipc
import struct

class PlcMemory:

    def __init__(self, memorykey, debug=False):
        self.memory = sysv_ipc.SharedMemory(memorykey)
        self.debug = debug
        self.Read()

    def Read(self):
        memoryread = self.memory.read()
        if self.debug:
            print >> sys.stderr, 'Received:' + binascii.hexlify(memoryread)
        memshape = '?' + '??????????????' + '???????????' + 'h'
        mem = struct.unpack(memshape, memoryread)
        self.mutex = mem[0]
        self.bool_in = [ i for i in mem[1:15] ]
        self.bool_out = [ i for i in mem[15:26] ]
        self.int_out = mem[::-1][0]

    def Refresh(self):
        self.Read()

    def Write(self):
        items = []
        for c in self.bool_in:
            items.append(c)

        packed = struct.pack('??????????????', *items)
        self.memory.write(packed, 1)

    def SetBoolInput(self, indice, value):
        self.bool_in[indice] = value
        self.Write()

    def SetBoolInputs(self, bool_in):
        self.bool_in = bool_in
        self.Write()

    def ReadBoolOutput(self, indice):
        return self.bool_out[indice]

    def ReadBoolOutputs(self):
        return self.bool_out

    def ReadIntOutput(self):
        return self.int_out

    def ResetInputs(self):
        for i in range(14):
            SetBoolInputs(i, False)
