CURRENT_DIRECTORY := $(shell pwd)
NAME = plc
TAG = latest
IMAGE = csaw/$(NAME)
CONTAINER = plc-container
HOSTPORT = 8080

build:
	@docker build -t $(IMAGE):$(TAG) $(CURRENT_DIRECTORY)

console:
	sudo docker exec -i -t $(CONTAINER) /bin/sh

delete:
	@docker stop $(CONTAINER)
	@sudo docker rm $(CONTAINER)

run-debug:
	@docker run -p $(HOSTPORT):80 --name $(CONTAINER) $(IMAGE):$(TAG)

run:
	@docker run -d -p $(HOSTPORT):80 --name $(CONTAINER) $(IMAGE):$(TAG)

start: build run
start-debug: build run-debug
restart: delete build run
restart-debug: delete build run-debug

stop-plc:
	@docker stop $(CONTAINER)
	@sudo docker rm $(CONTAINER)

start-plc:
ifeq ($(PROGRAM),)
	echo "Need to set PROGRAM"
else
	#Check if we can reach PROGRAM
	@touch $(PROGRAM)

	#Create
	@docker create -p $(HOSTPORT):80 --name $(CONTAINER) $(IMAGE):$(TAG)

	#Move program into container
	@docker cp $(PROGRAM) $(CONTAINER):/plc/openplc

	#Start
	@docker start $(CONTAINER)
endif
