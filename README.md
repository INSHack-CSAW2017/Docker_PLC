# DockerPLC

## Description
DockerPLC est un conteneur qui fait tourner un PLC
On lui donne un programme et il se lance,
puis on peut changer ses entrées et voir ses sorties via un serveur
jsonRPC sur le port 80

Dans le conteneur il y a:
- Le programme du PLC compilé avec Docker_PlcBuilder qui écrit dans une mémoire partagée
- Un serveur WEB Python Flask qui lis le contenu de la mémoire partagée
- Un serveur Apache qui sert de frontal au serveur Flask, pour plus de sureté de fonctionnement

## Build

    git clone https://gitlab.com/INSHack-CSAW2017/Docker_PLC
    cd Docker_PLC
    sudo make build -B


## Lancement
Si vous voulez le faire tourner c'est:

    sudo make start-plc PROGRAM=/Chemin/vers/votre/plc/compile/avec/Docker_PLCBuilder
    
Pour l'arreter:
    
    sudo make stop-plc



## Communication
Vous pouvez communiquer ensuite le PLC sur le port 8080 en localhost via le protocole jsonRPC

il vous faut le module jsonrpcclient sur python:

    sudo apt-get update && sudo apt-get install python3 python3-pip python3-requests
    sudo pip3 install jsonrpcclient
    

Ouvrez un shell python (tappez juste "python3")

    import jsonrpcclient

    #La vous aurez toutes les sorties du PLC
    jsonrpcclient.request('http://localhost:8080/', 'get_outputs')
     
    #Ici vous settez le booleen 3 sur vrai
    jsonrpcclient.request('http://localhost:8080/', 'set_bool_input', index=3,value=True)
     
Ce PLC prend l'entrée du booléen 3 et l'afficher en sortie 1

Si vous l'enlevez la sortie 1 reste pendant 2 secondes (c'est le programme de ce PLC qui fait ça)

## API
- `get_bool_outputs` => Récupère toutes les sorties sous forme d'un tableau de booleens
- `set_bool_input` => Parametres index (int) et value (bool), ecrit dans l'entrée (index) du PLC un booleen (value)
- `reset_inputs` => Remet toutes les entrées a False

