#!/bin/bash

cd /app
/plc/openplc &
uwsgi --ini /app.ini &
nginx -g 'daemon off;' 